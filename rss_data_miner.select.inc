<?php
/**
 * @file
 *   RSS Data Miner select functions.
 */

/**
 * Returns all node's nid in the specified content type.
 */
function _rss_data_miner_select_all_node_nid_by_type($type) {
  return db_select('node', 'n')
    ->fields('n', array('nid'))
    ->condition('type', $type)
    ->execute()
    ->fetchAllAssoc('nid');
}

/**
 * Returns the feeds_item's feed_nid by source url.
 */
function _rss_data_miner_select_feeds_nid_by_link($link) {
  return db_select('feeds_source', 'f')
    ->fields('f', array('feed_nid'))
    ->condition('source', $link)
    ->execute()
    ->fetchAllAssoc('feed_nid');
}

/**
 * Returns all unprocessed node's nid.
 */
function _rss_data_miner_select_unprocessed_forum_items() {
  return db_select('node', 'n')
    ->fields('n', array('nid'))
    ->condition('type', 'forum')
    ->condition('status', '0')
    ->execute()
    ->fetchAllAssoc('nid');
}

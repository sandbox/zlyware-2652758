<?php
/**
 * @file
 *   RSS Data Miner administration functions.
 */

/**
 * RSS Data Miner configuration form.
 */
function rss_data_miner_admin_settings($form, &$form_state) {
  $form['description'] = array(
    '#type' => 'item',
    '#title' => t('The English keyword to search for.'),
  );
  $form['keyword'] = array(
    '#type' => 'textfield',
    '#title' => t('The RSS Data Miner Keyword'),
    '#required' => TRUE,
    '#default_value' => variable_get('rss_data_miner_keyword', ''),
    '#description' => "Please enter the keyword to search for.",
    '#size' => 50,
    '#maxlength' => 50,
  );
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => 'Submit',
  );
  return $form;
}

/**
 * RSS Data Miner submit.
 */
function rss_data_miner_admin_settings_submit($form, &$form_state) {
  variable_set('rss_data_miner_keyword', $form_state['values']['keyword']);
  variable_set('rss_data_miner_languages', array());
}

<?php
/**
 * @file
 *   RSS Data Miner special setup functions.
 */

function rss_data_miner_final_setup() {
  
  // Set the link field in the forum content type.
  $instance = field_read_instance('node', 'link', 'forum');
  $instance['label'] = t('Original source');
  $instance['description'] = t('The source web address of the post.');
  $instance['required'] = TRUE;
  $instance['settings']['title'] = 'none';
  field_update_instance($instance);

  // Set the body field in the forum content type.
  $instance = field_read_instance('node', 'body', 'forum');
  $instance['required'] = TRUE;
  field_update_instance($instance);

  // Set the link field in the engine content type.
  $instance = field_read_instance('node', 'link', 'engine');
  $instance['label'] = t('Engine URL');
  $instance['description'] = t('The web address of the search engine. Use %q for kerword and %l for language, if exists.');
  $instance['required'] = TRUE;
  $instance['settings']['title'] = 'none';
  field_update_instance($instance);

  // Set OpenCalis fields in the forum content type.
  $entities = array();
  foreach (opencalais_get_all_entities() as $key => $entity) {
    $entities[$entity] = array('enabled' => '1');
  }
  $form_state = array(
    'values' => array(
      'content_type' => 'forum',
      'forum_autotagging' => '1',
      'config' => array(
        'entities' => $entities
      )
    ),
    'build_info' => array(
      'args' => array(
        'content_type' => 'forum',
      ),
    ),
  );
  include_once drupal_get_path('module', 'opencalais') . '/opencalais.admin.inc';
  drupal_form_submit('opencalais_add_fields_form', $form_state);
  
  // Set the language detection and selection form
  $form_state = array(
    'values' => array(
      'language' => array (
        'weight' => array (
          'locale-url' => '-8',
          'locale-session' => '-6',
          'locale-user' => '-4',
          'locale-browser' => '-2',
          'language-default' => '10',
        ),
        'enabled' => array (
          'locale-url' => '1',
          'locale-session' => '1',
          'locale-user' => '1',
          'locale-browser' => '1',
          'language-default' => '1',
        ),
      ),
      'language_content' => array (
        'weight' => array (
          'locale-url' => '-8',
          'locale-session' => '-6',
          'locale-user' => '-4',
          'locale-browser' => '-2',
          'locale-interface' => '8',
          'language-default' => '10',
        ),
        'enabled' => array (
          'locale-url' => '1',
          'locale-session' => '1',
          'locale-user' => '1',
          'locale-browser' => '1',
          'locale-interface' => '1',
          'language-default' => '1',
        ),
      ),
    )
  );
  include_once drupal_get_path('module', 'locale') . '/locale.admin.inc';
  drupal_form_submit('locale_languages_configure_form', $form_state);
  
  // Bulk generate URL aliases.
  $form_state = array(
    'values' => array(
      'update' => array(
        'node_pathauto_bulk_update_batch_process' => 'node_pathauto_bulk_update_batch_process',
        'taxonomy_pathauto_bulk_update_batch_process' => 'taxonomy_pathauto_bulk_update_batch_process',
        'user_pathauto_bulk_update_batch_process' => 'user_pathauto_bulk_update_batch_process',
        'forum_pathauto_bulk_update_batch_process' => 'forum_pathauto_bulk_update_batch_process',
      )
    )
  );
  include_once drupal_get_path('module', 'pathauto') . '/pathauto.admin.inc';
  drupal_form_submit('pathauto_bulk_update_form', $form_state);
  
  // Create Filtered HTML text format.
  $form_state = array(
    'values' => array(
      'name' => 'Filtered HTML',
      'format' => 'filtered_html',
      'roles' => array('1' => '1', '2' => '2', '3' => '3'),
      'filters' => array(
        'filter_html' => array(
          'status' => '1',
          'weight' => '0',
          'settings' => array(
            'allowed_html' => '<a> <em> <strong> <cite> <blockquote> <code> <ul> <ol> <li> <dl> <dt> <dd>',
            'filter_html_nofollow' => '1',
          ),
        ),
        'filter_autop' => array(
          'status' => '1',
          'weight' => '0',
        ),
        'filter_url' => array(
          'status' => '1',
          'weight' => '0',
          'settings' => array(
            'filter_url_length ' => '72',
          ),
        ),
        'filter_htmlcorrector' => array(
          'status' => '1',
          'weight' => '10',
        ),
        'filter_html_escape' => array(
          'weight' => '-10',
        ),
        'transliteration' => array(
          'weight' => '0',
          'settings' => array(
            'no_known_transliteration' => '?',
          ),
        ),
      ),
    )
  );
  include_once drupal_get_path('module', 'filter') . '/filter.admin.inc';
  drupal_form_submit('filter_admin_format_form', $form_state);
  

  // Create search engines.
  _rss_data_miner_create_engine('https://news.google.com/news?hl=%l&ned=us&ie=UTF-8&q="%q"&nolr=1&output=rss');
  _rss_data_miner_create_engine('http://www.dailymotion.com/rss/relevance/search/"%q"/1');
  _rss_data_miner_create_engine('https://www.google.com/alerts/feeds/13187294069914426107/17606858272331576586');
  _rss_data_miner_create_engine('http://blogs.icerocket.com/search?q="%q"&rss=1');
  _rss_data_miner_create_engine('http://feeds.delicious.com/v2/rss/tag/"%q"');
  _rss_data_miner_create_engine('http://api.flickr.com/services/feeds/photos_public.gne?tags="%q"&format=rss_200');
  
  // Set the keyword and start data mining...
  $form_state = array(
    'values' => array(
      'keyword' => 'handball',
    ),
  );
  include_once drupal_get_path('module', 'rss_data_miner') . '/rss_data_miner.admin.inc';
  drupal_form_submit('rss_data_miner_admin_settings', $form_state);
}

function _rss_data_miner_create_engine($url) {
  $node = new stdClass();
  $node->type = "engine";
  node_object_prepare($node);
  $node->language = LANGUAGE_NONE;
  $node->link[LANGUAGE_NONE][0]['url'] = $url;
  $node = node_submit($node);
  node_save($node);
}

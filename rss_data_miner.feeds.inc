<?php
/**
 * @file
 *   RSS Data Miner Feeds Importer.
 */

function rss_data_miner_ctools_plugin_api() {
  list($module, $api) = func_get_args();
  if ($module == 'feeds' && $api == 'feeds_importer_default') {
    return array("version" => 1);
  }
  elseif ($module == 'feeds_tamper' && $api == 'feeds_tamper_default') {
    return array("version" => 2);
  }
}

function rss_data_miner_feeds_importer_default() {
  $export = array();

  $feeds_importer = new stdClass();
  $feeds_importer->disabled = FALSE; /* Edit this to true to make a default feeds_importer disabled initially */
  $feeds_importer->api_version = 1;
  $feeds_importer->id = 'rss_data_miner';
  $feeds_importer->config = array(
    'name' => 'RSS Data Miner',
    'description' => 'The RSS Data Miner feed importer.',
    'fetcher' => array(
      'plugin_key' => 'FeedsHTTPFetcher',
      'config' => array(
        'auto_detect_feeds' => TRUE,
        'use_pubsubhubbub' => FALSE,
        'designated_hub' => '',
        'request_timeout' => NULL,
        'auto_scheme' => 'http',
        'accept_invalid_cert' => FALSE,
      ),
    ),
    'parser' => array(
      'plugin_key' => 'FeedsSyndicationParser',
      'config' => array(),
    ),
    'processor' => array(
      'plugin_key' => 'FeedsNodeProcessor',
      'config' => array(
        'expire' => '-1',
        'author' => 0,
        'authorize' => 0,
        'mappings' => array(
          0 => array(
            'source' => 'guid',
            'target' => 'guid',
            'unique' => 1,
          ),
          1 => array(
            'source' => 'title',
            'target' => 'title',
            'unique' => FALSE,
          ),
          2 => array(
            'source' => 'description',
            'target' => 'body',
            'unique' => FALSE,
          ),
          3 => array(
            'source' => 'url',
            'target' => 'link:url',
            'unique' => 1,
          ),
          4 => array(
            'source' => 'Blank source 1',
            'target' => 'taxonomy_forums',
            'unique' => FALSE,
          ),
        ),
        'update_existing' => '0',
        'update_non_existent' => 'skip',
        'input_format' => 'filtered_html',
        'skip_hash_check' => 0,
        'bundle' => 'forum',
        'insert_new' => 1,
        'language' => 'und',
      ),
    ),
    'content_type' => 'rss_feed',
    'update' => 0,
    'import_period' => '0',
    'expire_period' => 3600,
    'import_on_create' => 0,
    'process_in_background' => 0,
  );

  $export['node'] = $feeds_importer;
  return $export;
}

function rss_data_miner_feeds_tamper_default() {
  $export = array();

  $feeds_tamper = new stdClass();
  $feeds_tamper->disabled = FALSE; /* Edit this to true to make a default feeds_tamper disabled initially */
  $feeds_tamper->api_version = 2;
  $feeds_tamper->id = 'rss_data_miner-blank_source_1-php';
  $feeds_tamper->importer = 'rss_data_miner';
  $feeds_tamper->source = 'Blank source 1';
  $feeds_tamper->plugin_id = 'php';
  $feeds_tamper->settings = array(
    'php' => 'return _rss_data_miner_set_forum_taxonomy(item[\'url\']);',
  );
  $feeds_tamper->weight = 0;
  $feeds_tamper->description = 'Execute php code';

  $export[$feeds_tamper->id] = $feeds_tamper;

  $feeds_tamper = new stdClass();
  $feeds_tamper->disabled = FALSE; /* Edit this to true to make a default feeds_tamper disabled initially */
  $feeds_tamper->api_version = 2;
  $feeds_tamper->id = 'rss_data_miner-parent_nid-php';
  $feeds_tamper->importer = 'rss_data_miner';
  $feeds_tamper->source = 'parent:nid';
  $feeds_tamper->plugin_id = 'php';
  $feeds_tamper->settings = array(
    'php' => 'return _rss_data_miner_set_forum_taxonomy($item);',
  );
  $feeds_tamper->weight = 2;
  $feeds_tamper->description = 'Execute php code';

  $export[$feeds_tamper->id] = $feeds_tamper;

  $feeds_tamper = new stdClass();
  $feeds_tamper->disabled = FALSE; /* Edit this to true to make a default feeds_tamper disabled initially */
  $feeds_tamper->api_version = 2;
  $feeds_tamper->id = 'rss_data_miner-description-required';
  $feeds_tamper->importer = 'rss_data_miner';
  $feeds_tamper->source = 'description';
  $feeds_tamper->plugin_id = 'required';
  $feeds_tamper->settings = array(
    'invert' => 0,
    'log' => 1,
  );
  $feeds_tamper->weight = 0;
  $feeds_tamper->description = 'Required field';

  $export[$feeds_tamper->id] = $feeds_tamper;

  $feeds_tamper = new stdClass();
  $feeds_tamper->disabled = FALSE; /* Edit this to true to make a default feeds_tamper disabled initially */
  $feeds_tamper->api_version = 2;
  $feeds_tamper->id = 'rss_data_miner-title-required';
  $feeds_tamper->importer = 'rss_data_miner';
  $feeds_tamper->source = 'title';
  $feeds_tamper->plugin_id = 'required';
  $feeds_tamper->settings = array(
    'invert' => 0,
    'log' => 1,
  );
  $feeds_tamper->weight = 1;
  $feeds_tamper->description = 'Required field';

  $export[$feeds_tamper->id] = $feeds_tamper;

  $feeds_tamper = new stdClass();
  $feeds_tamper->disabled = FALSE; /* Edit this to true to make a default feeds_tamper disabled initially */
  $feeds_tamper->api_version = 2;
  $feeds_tamper->id = 'rss_data_miner-title-strip_tags';
  $feeds_tamper->importer = 'rss_data_miner';
  $feeds_tamper->source = 'title';
  $feeds_tamper->plugin_id = 'strip_tags';
  $feeds_tamper->settings = array(
    'allowed_tags' => '',
  );
  $feeds_tamper->weight = 0;
  $feeds_tamper->description = 'Strip tags';

  $export[$feeds_tamper->id] = $feeds_tamper;

  $feeds_tamper = new stdClass();
  $feeds_tamper->disabled = FALSE; /* Edit this to true to make a default feeds_tamper disabled initially */
  $feeds_tamper->api_version = 2;
  $feeds_tamper->id = 'rss_data_miner-url-php';
  $feeds_tamper->importer = 'rss_data_miner';
  $feeds_tamper->source = 'url';
  $feeds_tamper->plugin_id = 'php';
  $feeds_tamper->settings = array(
    'php' => 'return substr($field, strrpos($field, \'http\'));',
  );
  $feeds_tamper->weight = 2;
  $feeds_tamper->description = 'Execute php code';

  $export[$feeds_tamper->id] = $feeds_tamper;

  $feeds_tamper = new stdClass();
  $feeds_tamper->disabled = FALSE; /* Edit this to true to make a default feeds_tamper disabled initially */
  $feeds_tamper->api_version = 2;
  $feeds_tamper->id = 'rss_data_miner-url-required';
  $feeds_tamper->importer = 'rss_data_miner';
  $feeds_tamper->source = 'url';
  $feeds_tamper->plugin_id = 'required';
  $feeds_tamper->settings = array(
    'invert' => 0,
    'log' => 1,
  );
  $feeds_tamper->weight = 3;
  $feeds_tamper->description = 'Required field';

  $export[$feeds_tamper->id] = $feeds_tamper;

  $feeds_tamper = new stdClass();
  $feeds_tamper->disabled = FALSE; /* Edit this to true to make a default feeds_tamper disabled initially */
  $feeds_tamper->api_version = 2;
  $feeds_tamper->id = 'rss_data_miner-url-unique';
  $feeds_tamper->importer = 'rss_data_miner';
  $feeds_tamper->source = 'url';
  $feeds_tamper->plugin_id = 'unique';
  $feeds_tamper->settings = array();
  $feeds_tamper->weight = 4;
  $feeds_tamper->description = 'Unique';

  $export[$feeds_tamper->id] = $feeds_tamper;

  return $export;
}

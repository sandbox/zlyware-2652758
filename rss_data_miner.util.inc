<?php
/**
 * @file
 *   RSS Data Miner utility functions.
 */

/**
 * RSS Data Miner setup all.
 *
 * - Translate multilingual variables.
 * - Create multilingual RSS feeds.
 */
function _rss_data_miner_setup_all() {
  if (_rss_data_miner_translate_variables() && _rss_data_miner_create_feeds()) {
    variable_set('rss_data_miner_languages', locale_language_list());
    watchdog('rss_data_miner', '_rss_data_miner_setup_all set languages: @lang', array('@lang' => serialize(locale_language_list())));
  } else {
    variable_set('rss_data_miner_languages', array());
    watchdog('rss_data_miner', '_rss_data_miner_setup_all cleared all languages.', array(), WATCHDOG_ERROR);
  }
  cache_clear_all();
}

/**
 * Translate multilingual variables.
 */
function _rss_data_miner_translate_variables() {
  $return = TRUE;
  db_query('DELETE FROM variable_store;');
  foreach (variable_get('variable_realm_list_language', array()) as $key => $variable) {
    foreach (locale_language_list() as $langcode => $langname) {
      if ($langcode == 'en') {
        _rss_data_miner_write_variable($variable, variable_get($variable), $langcode);
      } else {
        $translated = microsoft_translator_api_translate(variable_get($variable), 'en', $langcode, TRUE);
        if ($translated) {
          _rss_data_miner_write_variable($variable, $translated, $langcode);
          watchdog('rss_data_miner', 'Translated variable: @variable @langname @translated', array('@variable' => $variable, '@langname' => $langname, '@translated' => $translated));
        } else {
          watchdog('rss_data_miner', 'Translation failed: @variable @langname', array('@variable' => $variable, '@langname' => $langname), WATCHDOG_ERROR);
          $return = FALSE;
        }
      }
    }
  }
  return $return;
}

/**
 * Write a multilingual variable to the database.
 */
function _rss_data_miner_write_variable($variable, $translated, $langcode) {
  return db_insert('variable_store')
      ->fields(array(
          'realm' => 'language',
          'realm_key' => $langcode,
          'name' => $variable,
          'value' => $translated,
          'serialized' => 0,
        ))
      ->execute();
}

/**
 * - Create multilingual RSS feeds.
 */
function _rss_data_miner_create_feeds() {
  $return = TRUE;
  if ($keyword = variable_get('rss_data_miner_keyword', '')) {
    $engines = _rss_data_miner_select_all_node_nid_by_type('engine');
    foreach ($engines as $engine) {
      $node_engine = node_load($engine->nid);
      $link = $node_engine->link[LANGUAGE_NONE][0]['url'];
      foreach (locale_language_list() as $langcode => $langname) {
        if ($langcode == 'en') {
          _rss_data_miner_create_feed(str_replace('%l', $langcode, str_replace('%q', $keyword, $link)));
        } else {
          $translated = strtolower(microsoft_translator_api_translate($keyword, 'en', $langcode, TRUE));
          if ($translated) {
            _rss_data_miner_create_feed(str_replace('%l', $langcode, str_replace('%q', $translated, $link)));
          } else {
            watchdog('rss_data_miner', 'Translation failed: @link @langname', array('@link' => $link, '@langname' => $langname), WATCHDOG_ERROR);
            variable_set('rss_data_miner_languages', array());            
            $return = FALSE;
          }
        }
      }
    }
  }
  return $return;
}

/**
 * Create a RSS feed, if it does not exists.
 */
function _rss_data_miner_create_feed($url) {
  if (count(_rss_data_miner_select_feeds_nid_by_link($url)) == 0) {
    $node = new stdClass();
    $node->type = 'rss_feed';
    node_object_prepare($node);
    $node->language = LANGUAGE_NONE;
    $node->feeds['FeedsHTTPFetcher']['source'] = $url;
    node_save($node);
    watchdog('rss_data_miner', 'A new RSS feed is successfully created: @url', array('@url' => $url));
  }
  else {
    watchdog('rss_data_miner', 'A RSS feed creation with this URL is omitted, because it is already exists: @url', array('@url' => $url));
  }
}

/**
 * Searching for new languages.
 */
function _rss_data_miner_check_languages() {
  $saved_languages = variable_get('rss_data_miner_languages');
  foreach (locale_language_list() as $langcode => $langname) {
    if (!array_key_exists($langcode, $saved_languages)) {
      watchdog('rss_data_miner', 'New language found: @langname.', array('@langname' => $langname));
      _rss_data_miner_setup_all();
      break;
    }
  }
}

/**
 * Search all unpublished forum topic, and save it again. Called by hook_cron.
 */
function _rss_data_miner_process_feed_items() {
  foreach (_rss_data_miner_select_unprocessed_forum_items() as $forum) {
    node_save(node_load($forum->nid));
  }
}

/**
 * Process a forum topic before save, if that is unpublished. Called by hook_entity_presave.
 */
function _rss_data_miner_forum_presave($node) {
  watchdog('rss_data_miner_presave', '!name: !export', array('!export' => kprint_r($node, TRUE, 'node'), '!name' => 'node'), WATCHDOG_DEBUG);
  if ($node->status == NODE_NOT_PUBLISHED) {
    $node->forum_tid = $node->taxonomy_forums[LANGUAGE_NONE][0]['tid'] = 1;
    if ($term = _rss_data_miner_set_forum_taxonomy($node->link[LANGUAGE_NONE][0]['url'])) {
      $node->forum_tid = $node->taxonomy_forums[LANGUAGE_NONE][0]['tid'] = $term;
      $node->status = NODE_PUBLISHED;
      $node->comment = COMMENT_NODE_OPEN;
      $node->log = t('Processed by RSS data miner.');

      // Find new RSS channels.
      $request = drupal_http_request($node->link[LANGUAGE_NONE][0]['url']);
      if ($request->code == '200') {
        $html_obj = new simple_html_dom();
        $html_obj->load($request->data);
        foreach ($html_obj->find('link') as $link) {
          if ($link->rel == 'alternate' && $link->type == 'application/rss+xml') {
            watchdog('rss_data_miner', 'A possible new RSS feed found: @url', array('@url' => $link->href));
            //_rss_data_miner_create_feed($link->href);
          }
        }
      } else {
        watchdog('rss_data_miner', 'drupal_http_request code for find new channel at @source is @code.', array('@source' => $node->link[LANGUAGE_NONE][0]['url'], '@code' => $request->code), WATCHDOG_ERROR);
      }
    }
  }
}

/**
 * Create or get a taxonomy term for the forum topic.
 */
function _rss_data_miner_set_forum_taxonomy($url) {
  watchdog('rss_data_miner', '_rss_data_miner_set_forum_taxonomy called for @url.', array('@url' => $url));
  $source = parse_url($url, PHP_URL_SCHEME) . '://' . parse_url($url, PHP_URL_HOST);
  watchdog('rss_data_miner', 'drupal_http_request @source', array('@source' => $source));
  $request = drupal_http_request($source);
  if ($request->code == '200') {
    $html_obj = new simple_html_dom();
    $html_obj->load($request->data);
    foreach ($html_obj->find('title') as $title) {
      $source = strip_tags($title);
    }
  } else {
    $source = parse_url($url, PHP_URL_HOST);
    watchdog('rss_data_miner', 'drupal_http_request code for @source is @code.', array('@source' => $source, '@code' => $request->code), WATCHDOG_ERROR);
  }
  $term = taxonomy_get_term_by_name($source, 'forums');
  if (count($term) == 0) {
    $term = new stdClass();
    $term->name = $source;
    $term->vid = taxonomy_vocabulary_machine_name_load('forums')->vid;
    taxonomy_term_save($term);
  } else {
    $term = $term[key($term)];
  }
  return $term->tid;
}
